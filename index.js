(function() {
    var deleteCookie, getCookie, getCookieValue, setCookie;

    setCookie = function(cname, cvalue, exdays) {
        var d, expires;
        d = new Date;
        d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
        expires = 'expires=' + d.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    };

    getCookie = function(cname) {
        var c, ca, decodedCookie, i, name;
        name = cname + '=';
        decodedCookie = decodeURIComponent(document.cookie);
        ca = decodedCookie.split(';');
        i = 0;
        while (i < ca.length) {
            c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
            i++;
        }
    };

    getCookieValue = function(cname) {
        var cvalue;
        cvalue = getCookie(cname);
        if (cvalue !== '') {
            return cvalue;
        } else {
            return false;
        }
    };

    deleteCookie = function(name) {
        return setCookie(name, "", -1);
    };

}).call(this);